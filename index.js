function printInfo() {

    let nickname = prompt("Enter your Nickname");
    console.log("Hi, " + nickname)

    // return printInfo();

}

//printInfo();

function printName(name) {
    console.log("My name is " + name);
}

printName("Juan");
printName("John");

//[SECTION] Parameters and Arguments

//Parameter -> "name" is a parameter -> it acts as a container inside a function -> used to store info provided to a function

//Argument -> "Juana" and "John" -> info or data provided in the function

//Variables can be passed on an argument

let sampleVariable = "You";

printName(sampleVariable);
printName();

//CHECK IF A SPECIFIC NUMBER IS DIVISIBLE BY 8

function checkDivisibilityBy8(num) {
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

//[SECTION] Function as Argument
//Function parameters can accept other functions as arguments

function argumentFunction() {
    console.log("This function was passed as an argument before a message was printed.");
}

function invokeFunciton(argumentFunction) {
    argumentFunction();
}

invokeFunciton(argumentFunction);

console.log(argumentFunction)

//[SECTION] Using Multiple Parameters

function createFullName(firstName, middleName, lastName) {
    console.log("My Full Name is " + firstName + " " + middleName + " " + lastName);
}

createFullName("Romenick", "Barredo", "Garcia");

//Using variable as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

//[SECTION] The return statement
//"return" allows an output value from a funciton to be passed to the line/block.

function returnFullName(firstName, middleName, lastName) {
    /*  let fullName = firstName + " " + middleName + " " + lastName;
     return fullName */

    return firstName + " " + middleName + " " + lastName;
    console.log("This is printed inside a function");
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);
console.log("My name is " + completeName)

function printPlayerInfo(userName, level, job) {
    console.log("Username: " + userName);
    console.log("Level: " + level)
    console.log("Job: " + job)
}

let user1 = printPlayerInfo("knight_white", 95, "Palladin");
console.log(user1); //returns undefined